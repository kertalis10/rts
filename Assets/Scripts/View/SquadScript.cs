﻿using UnityEngine;

public class SquadScript : MonoBehaviour
{
    public System.Action<string> NewCollision;

    public void OnTriggerEnter2D(Collider2D other)
    {        
        NewCollision?.Invoke(other.tag);
    }
}
