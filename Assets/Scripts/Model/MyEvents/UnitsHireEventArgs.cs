﻿using System;

namespace Assets.Scripts.Model.MyEvents
{
    public class UnitsHireEventArgs:EventArgs
    {
        public int Att { get; private set; }
        public int Fast { get; private set; }
        public int Def { get; private set; }

        public UnitsHireEventArgs(int a, int f, int d)
        {
            Att = a;
            Fast = f;
            Def = d;
        }

    }
}
