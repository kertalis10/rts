﻿using Assets.Scripts.Model.MyEvents;
using Start.Model;
using Start.MyEvents;
using Start.View;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Start
{
    class GameModel
    {
        readonly IView view;

        int[,] gameField;
        int gameStep;       
        List<Base> bases;
        List<Bot> bots;
        List<Squad> attSquads;
        Random rand;

        public GameModel(IView _view)
        {
            view = _view;

            view.StartGame += InitGameModel;
            view.GameStep += GameStep;
            view.HouseUp += HouseUp;
            view.BarracksUp += BarracksUp;
            view.WallUp += WallUp;
            view.FacktoryUp += FactoryUp;
            view.PortalUp += PortalUp;
            view.ExpandBase += ExpandBase;
            view.HireUnits += HireUnits;
            view.PlayerAttack += PlayerAttack;
            view.NewBattle += NewBattle;
            view.NewDeal += NewDeal;

            view.Run();
        }

        public void InitGameModel(object sender, StartEventArgs e)
        {
            rand = new Random();
            gameField = new int[e.Height, e.Width];
            gameStep = e.GameStep;           

            CreateGameField(e.CountOfEnemies);

            view.DrawField(gameField, ReturnInfo(), ReturnUnitHireInfo());            
        }

        private void CreateGameField(int countOfEnemies)
        {
            int count = 0;

            int height = gameField.GetLength(0);
            int width = gameField.GetLength(1);

            bases = new List<Base>();
            bots = new List<Bot>();
            attSquads = new List<Squad>();

            while (count <= countOfEnemies)
            {
                int h = rand.Next(0, height - 1);
                int w = rand.Next(0, width - 1);

                if (gameField[h, w] == 0)
                {
                    bases.Add(new Base(h, w, count));

                    if (count > 0)
                        bots.Add(new Bot(bases[bases.Count - 1], count + 1, gameField, rand));

                    count++;
                    gameField[h, w] = count;
                    if (h > 0)
                    {
                        gameField[h - 1, w] = 100;
                        if (w > 0)
                            gameField[h - 1, w - 1] = 100;
                    }
                    if (h < height - 1)
                    {
                        gameField[h + 1, w] = 100;
                        if (w < width - 1)
                            gameField[h + 1, w + 1] = 100;
                    }
                    if (w > 0)
                    {
                        gameField[h, w - 1] = 100;
                        if (h < height - 1)
                            gameField[h + 1, w - 1] = 100;
                    }
                    if (w < width - 1)
                    {
                        gameField[h, w + 1] = 100;
                        if (h > 0)
                            gameField[h - 1, w + 1] = 100;
                    }
                }
            }

            foreach (Bot b in bots)
                b.SquadAttack += NewAttack;
        }

        private void GameStep(object sender, EventArgs e)
        {
            //Игрок
            bases[0].Step();

            //Боты
            foreach (Bot bot in bots)
                bot.GameStep(rand);            

            //Обновление представления
            view.CheckTable(gameField, ReturnInfo(), ReturnUnitHireInfo());
        }
        
        private void NewAttack(object sender, SquadEventArgs e)
        {
            if (e.Sq.Att > 0)
            {
                int curX = e.Sq.TargetPos.X;
                int curY = e.Sq.TargetPos.Y;
                int x = 0;
                int y = 0;
                double currDistance = 1000;
                double distance = 0;

                //Выбор атакуемой базы 
                foreach (Base b in bases)
                {
                    if(e.Sq.BaseNum != b.BaseNum)
                        foreach (Point p in b.Points)
                        {
                            distance = Math.Sqrt(Math.Abs(Math.Pow(curX - p.X, 2) - Math.Pow(curY - p.Y, 2)));
                            if (distance != 0 && currDistance > distance)
                            {
                                currDistance = distance;
                                x = p.X;
                                y = p.Y;
                            }
                        }
                }

                attSquads.Add(e.Sq);
                view.NewAttack(e.Sq.BasePos.X, e.Sq.BasePos.Y, x, y, e.Sq.BaseNum, e.Sq.Speed);               
            }
        }

        private void PlayerAttack(object sender, PlayerAttackEventArgs e)
        {
            Squad s = bases[0].RisePlayerAttack(e.Att, e.Fast, e.Def);

            if(s != null)
            {
                attSquads.Add(s);
                view.NewAttack(s.BasePos.X, s.BasePos.Y, e.X, e.Y, s.BaseNum, s.Speed);
                view.CheckTable(gameField, ReturnInfo(), ReturnUnitHireInfo());
            }
        }

        //Битва за базу или возвращение свою базу
        private void NewBattle(object sender, BattleEventArgs e)
        {     
            Squad s = attSquads.Where(x => x.BaseNum == e.SquadNum).First();
            Base b = bases.Where(x => x.BaseNum == e.BaseNum - 1).First();


            if (e.SquadNum == e.BaseNum - 1)
            {
                b.WinSquadReturn();
                b.BaseSquad += s;
                b.IsSquadOnTheRaid = false;
                attSquads.Remove(s);
                view.DeleteSquad(e.SquadNum);               
            }
            else
            {

                if (s.Att > b.BaseSquad.Def)
                {
                    if (e.BaseNum == 0)
                        view.Lose();

                    s.ReciveDamageAtt(b.BaseSquad.Def);

                    int temp = 0;

                    foreach (Bot bot in bots)
                    {
                        bot.Stop(e.BaseNum - 1);
                        if (bot.IsStoped == false)
                            temp++;
                    }

                    if (temp == 0)
                        view.Win();

                    for (int i = 0; i < gameField.GetLength(0); i++)
                        for (int j = 0; j < gameField.GetLength(1); j++)
                            if (gameField[i, j] == e.BaseNum)
                                gameField[i, j] = 0;
                }
                else
                {
                    bases[e.SquadNum].IsSquadOnTheRaid = false;
                    b.BaseSquad.ReciveDamageDef(s.Att);
                    attSquads.Remove(s);
                    view.DeleteSquad(e.SquadNum);
                }
            }   

            view.CheckTable(gameField, ReturnInfo(), ReturnUnitHireInfo());
        }

        private void NewDeal(object sender, ResourcesTradeArgs e)
        {
            bases[0].Deal(e.BuyCount, e.SellCount);

            view.CheckTable(gameField, ReturnInfo(), ReturnUnitHireInfo());
        }

        private void ExpandBase(object sender, EventArgs e)
        {
            if (bases[0].IsCanAddBase())
            {
                List<Point> temp = new List<Point>();

                foreach (Point p in bases[0].Points)
                {
                    if (p.X - 1 >= 0 && (gameField[p.X - 1, p.Y] == 0 || gameField[p.X - 1, p.Y] > 6))
                        temp.Add(new Point(p.X - 1, p.Y));
                    if (p.Y - 1 >= 0 && (gameField[p.X, p.Y - 1] == 0 || gameField[p.X, p.Y - 1] > 6))
                        temp.Add(new Point(p.X, p.Y - 1));
                    if (p.X + 1 < gameField.GetLength(0) && (gameField[p.X + 1, p.Y] == 0 || gameField[p.X + 1, p.Y] > 6))
                        temp.Add(new Point(p.X + 1, p.Y));
                    if (p.Y + 1 < gameField.GetLength(1) && (gameField[p.X, p.Y + 1] == 0 || gameField[p.X, p.Y + 1] > 6))
                        temp.Add(new Point(p.X, p.Y + 1));
                }

                if (temp.Count > 0)
                {
                    Point selected = temp[new Random().Next(0, temp.Count - 1)];
                    bases[0].AddBase(selected.X, selected.Y);
                    gameField[selected.X, selected.Y] = 1;
                    view.CheckTable(gameField, ReturnInfo(), ReturnUnitHireInfo());
                }
            }
        }

        private void HouseUp(object sender, EventArgs e)
        {
            if(bases[0].HouseUp())
                view.CheckTable(gameField, ReturnInfo(), ReturnUnitHireInfo());
        }

        private void BarracksUp(object sender, EventArgs e)
        {
            if (bases[0].BarracksUp())
                view.CheckTable(gameField, ReturnInfo(), ReturnUnitHireInfo());
        }

        private void WallUp(object sender, EventArgs e)
        {
            if (bases[0].WallUp())
                view.CheckTable(gameField, ReturnInfo(), ReturnUnitHireInfo());
        }

        private void FactoryUp(object sender, EventArgs e)
        {
            if (bases[0].FacktoryUp())
                view.CheckTable(gameField, ReturnInfo(), ReturnUnitHireInfo());
        }

        private void PortalUp(object sender, EventArgs e)
        {
            if (bases[0].PortalUp())
                view.CheckTable(gameField, ReturnInfo(), ReturnUnitHireInfo());
        }

        private void HireUnits(object sender, UnitsHireEventArgs e)
        {
            if (e.Att > 0)
                bases[0].AddUnits(new AttUnit(), e.Att);
            if (e.Fast > 0)
                bases[0].AddUnits(new FastUnit(), e.Fast);
            if (e.Def > 0)
                bases[0].AddUnits(new DefUnit(), e.Def);

            view.CheckTable(gameField, ReturnInfo(), ReturnUnitHireInfo());
        }


        private string[] ReturnInfo()
        {
            string[] s = new string[7];

            foreach (Bot bot in bots)
                s[0] += bot.ToString();

            s[1] = bases[0].ResourcesInfo();
            s[2] = "House:      " + bases[0].HouseLvl.Min() + "(" + bases[0].HouseLvl.Sum() + ")";
            s[3] = "Barracks:  " + bases[0].BarracksLvl.Min() + "(" + bases[0].BarracksLvl.Sum() + ")";
            s[4] = "Wall:          " + bases[0].WallLvl;
            s[5] = "Factory:     " + bases[0].FactoryLvl;
            s[6] = "Portal:        " + bases[0].PortalLvl;           

            return s;
        }

        private int[] ReturnUnitHireInfo()
        {
            int[] temp = new int[7];

            temp[0] = bases[0].Credits;
            temp[1] = bases[0].Goods;
            temp[2] = bases[0].PeoplesLimit - bases[0].BaseSquad.ListOfUnits.Count;
            temp[3] = bases[0].UnitsLimit - bases[0].BaseSquad.ListOfUnits.Count;
            temp[4] = bases[0].BaseSquad.ListOfUnits.Where(x => x.Att == 5).Count(); //Att
            temp[5] = bases[0].BaseSquad.ListOfUnits.Where(x => x.Att == 3).Count(); //Fast
            temp[6] = bases[0].BaseSquad.ListOfUnits.Where(x => x.Att == 2).Count(); //Def

            return temp;
        }
    }



}
