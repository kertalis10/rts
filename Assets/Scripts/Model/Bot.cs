﻿using Start.MyEvents;
using System;
using System.Collections.Generic;

namespace Start.Model
{
    class Bot
    {
        public event EventHandler<SquadEventArgs> SquadAttack;
        Base bs;
        int[,] gameField;
        int num;
        int type;
        int turn;      
        public bool IsStoped { get; private set; }

        public Bot(Base _bs, int _num, int[,] gf, Random rand)
        {
            bs = _bs;
            num = _num;
            gameField = gf;
            type = rand.Next(1, 4);
            turn = 0;
            IsStoped = false;
        }

        public void GameStep(Random rand)
        {
            if (!IsStoped)
            {
                turn++;

                if (type == 1) AgrBotStep(rand);
                else if (type == 2) MiddleBotStep(rand);
                else DefBotStep(rand);

                bs.Step();
            }
        }
        //Отключение бота
        public void Stop(int num)
        {
            if (bs.BaseNum == num)
                IsStoped = true;
        }

        void AgrBotStep(Random rand)
        {
            if (rand.Next(1, 10) <= 3 && bs.IsCanAddBase())
                AddBase();          

            if (rand.Next(1, 10) <= 3)
                bs.AddUnits(new AttUnit(), 100);
            else if(rand.Next(1, 10) <= 4)
                bs.AddUnits(new DefUnit(), 100);        

            if (rand.Next(1, 10) <= 3 && bs.BaseSquad.Att > 450 * bs.Points.Count && !bs.IsSquadOnTheRaid)
            {
                SquadAttack?.Invoke(this, new SquadEventArgs(bs.BaseSquad.NewSquad(1000, 1000, 0)));                
            }

        }

        void MiddleBotStep(Random rand)
        { 
            if (rand.Next(1, 10) <= 5 && bs.IsCanAddBase())
                AddBase();         

            if (rand.Next(1, 10) <= 1)
                bs.AddUnits(new AttUnit(), 100);
            else if (rand.Next(1, 10) <= 4)
                bs.AddUnits(new DefUnit(), 100);           

            if (rand.Next(1, 10) <= 1 && bs.BaseSquad.Att > 450 * bs.Points.Count && !bs.IsSquadOnTheRaid)
            {
                SquadAttack?.Invoke(this, new SquadEventArgs(bs.BaseSquad.NewSquad(1000, 1000, 0)));                
            }
        }

        void DefBotStep(Random rand)
        {
            if (rand.Next(1, 10) <= 7 && bs.IsCanAddBase())
                AddBase();          

            if (rand.Next(1, 10) <= 3)
                bs.AddUnits(new DefUnit(), 100);    
        }

        //Расширение базы, захват новой клетки
        void AddBase()
        {
            List<Point> temp = new List<Point>();

            foreach (Point p in bs.Points)
            {
                if (p.X - 1 >= 0 && (gameField[p.X - 1, p.Y] == 0 || gameField[p.X - 1, p.Y] > 6))
                    temp.Add(new Point(p.X - 1, p.Y));
                if (p.Y - 1 >= 0 && (gameField[p.X, p.Y - 1] == 0 || gameField[p.X, p.Y - 1] > 6))
                    temp.Add(new Point(p.X, p.Y - 1));
                if (p.X + 1 < gameField.GetLength(0) && (gameField[p.X + 1, p.Y] == 0 || gameField[p.X + 1, p.Y] > 6))
                    temp.Add(new Point(p.X + 1, p.Y));
                if (p.Y + 1 < gameField.GetLength(1) && (gameField[p.X, p.Y + 1] == 0 || gameField[p.X, p.Y + 1] > 6))
                    temp.Add(new Point(p.X, p.Y + 1));
            }

            if (temp.Count > 0)
            {
                Point selected = temp[new Random(num).Next(0, temp.Count)];
                bs.AddBase(selected.X, selected.Y);
                gameField[selected.X, selected.Y] = num;
            }
        }

        public override string ToString()
        {
            return "T: " + type + " "  + bs.ToString();
        }

    }
}
