﻿using System;

namespace Start.MyEvents
{
    public class StartEventArgs : EventArgs
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int CountOfEnemies { get; private set; }
        public int GameStep { get; private set; }
               
        public StartEventArgs(int width, int height, int count, int step)
        {
            Width = width;
            Height = height;
            CountOfEnemies = count;
            GameStep = step;
        }
    }
}
