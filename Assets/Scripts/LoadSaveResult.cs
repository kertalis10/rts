﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Assets.Scripts
{
    public static class LoadSaveResult
    {
        [System.Serializable]
        public struct PlayerInfo
        {
            public string name;
            public string result;
            public int bots;
            public float time;

            public PlayerInfo(string _name, string _result, int _bots, float _time)
            {
                name = _name;
                result = _result;
                time = _time;
                bots = _bots;
            }

            public override string ToString()
            {
                return string.Format("{0}\t\t{1}\t\t{2}\t\t{3}", name, result, bots, ((int)MenuArgs.time / 60).ToString() + "." + (MenuArgs.time % 60).ToString());
            }
        }

        //Загрузка результатов из файла
        public static List<T> LoadResults<T>(string path)
        {
            List<T> list;

            System.Xml.Serialization.XmlSerializer formatter = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));

            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Open))
                    list = (List<T>)formatter.Deserialize(fs);
            }
            catch
            {
                list = new List<T>();
            }


            return list;
        }

        //Сохранение результатов в файл
        public static void SaveResults<T>(List<T> list, string path)
        {
            System.Xml.Serialization.XmlSerializer formatter = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));

            try
            {
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                    formatter.Serialize(fs, list);
            }
            catch
            {
            }
        }
    }
}
