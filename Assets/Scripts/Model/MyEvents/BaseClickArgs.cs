﻿using System;

namespace Assets.Scripts.Model.MyEvents
{
    public class BaseClickArgs : EventArgs
    {
        public int Num { get; private set; }
        public float X { get; private set; }
        public float Y { get; private set; }


        public BaseClickArgs(int n, float x, float y)
        {
            Num = n;
            X = x;
            Y = y;
        }
         
    }
}
