﻿using Assets.Scripts.Model.MyEvents;
using System;
using UnityEngine;

public class OnBaseCliskScript : MonoBehaviour
{
    private event EventHandler<BaseClickArgs> _click;

    public event EventHandler<BaseClickArgs> Click
    {
        add
        {
            if (_click == null)
                _click += value;
        }
        remove
        {
            _click -= value;
        }
    }

    private void OnMouseDown()
    {
        _click.Invoke(this, new BaseClickArgs(Int32.Parse(this.tag), this.transform.position.x, this.transform.position.y));
    }
}
