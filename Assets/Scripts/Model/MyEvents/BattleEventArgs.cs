﻿using System;

namespace Assets.Scripts.Model.MyEvents
{
    public class BattleEventArgs
    {
        public int SquadNum { get; private set; }
        public int BaseNum { get; private set; }

        public BattleEventArgs(int sNum, int bNum)
        {
            SquadNum = sNum;
            BaseNum = bNum;
        }
    }
}
