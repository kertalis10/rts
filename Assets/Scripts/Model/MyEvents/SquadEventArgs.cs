﻿using Start.Model;
using System;

namespace Start.MyEvents
{
    class SquadEventArgs:EventArgs
    {
        public Squad Sq { get; private set; }

        public SquadEventArgs(Squad s)
        {
            Sq = s;
        }
    }
}
