﻿using System;

namespace Start.Model
{
    public abstract class Unit
    {
        public int Att { get; protected set; }
        public int Def { get; protected set; }
        public int Speed { get; protected set; }

        public Unit()
        {
        }
    }

    public class AttUnit : Unit
    {
        public AttUnit()
        {
            Att = 5;
            Def = 1;
            Speed = 2;
        }
    }

    public class FastUnit : Unit
    {
        public FastUnit()
        {
            Att = 3;
            Def = 1;
            Speed = 4;
        }
    }

    public class DefUnit : Unit
    {
        public DefUnit()
        {
            Att = 2;
            Def = 4;
            Speed = 2;
        }
    }
}
