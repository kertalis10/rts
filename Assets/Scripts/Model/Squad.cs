﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Start.Model
{
    class Squad
    {
        public int BaseNum { get; private set; }
        public Point BasePos { get; private set; }
        public Point TargetPos { get; private set; }
        public Point CurrentPos { get; private set; }
        public List<Unit> ListOfUnits { get; private set; }
        public int Speed { get { return ListOfUnits.Min(x => x.Speed); } }
        public float Att { get { return ListOfUnits.Sum(x => x.Att) + ListOfUnits.Count * attDefBuff; } }
        public float Def { get { return ListOfUnits.Sum(x => x.Def) + ListOfUnits.Count * (attDefBuff + baseDefBuff); } }

        float attDefBuff, baseDefBuff;

        public Squad(Point bp, Point tp, Point cp, int bNum)
        {
            attDefBuff = baseDefBuff = 0;
            BasePos = bp;
            TargetPos = tp;
            CurrentPos = cp;
            BaseNum = bNum;
            ListOfUnits = new List<Unit>();
        }

        public void SetTarget(Point p)
        {
            TargetPos = p;
        }

        public void SetCurrentPos(Point p)
        {
            CurrentPos = p;
        }

        public void AddUnits(Unit unit, int count)
        {
            for (int i = 0; i < count; i++)
                ListOfUnits.Add(unit);

            ListOfUnits = ListOfUnits.OrderByDescending(x => x.Att).ToList();
        }

        public static Squad operator +(Squad s1, Squad s2)
        {
            for (int i = 0; i < s2.ListOfUnits.Count; i++)
                s1.AddUnits(s2.ListOfUnits[i], 1);

            s2.ListOfUnits.Clear();

            return s1;
        }

        public Squad NewSquad(int att, int fast, int def)
        {
            Squad temp = new Squad(this.BasePos, this.TargetPos, this.CurrentPos, this.BaseNum);

            int attCount = ListOfUnits.Where(x => x.Att == 5).Count();
            int fastCount = ListOfUnits.Where(x => x.Att == 3).Count();
            int defCount = ListOfUnits.Where(x => x.Att == 2).Count();

            if (att > attCount) att = attCount;
            if (fast > fastCount) fast = fastCount;
            if (def > defCount) def = defCount;

            if (att > 0)
                temp.AddUnits(ListOfUnits[0], att);

            if (fast > 0)
                temp.AddUnits(ListOfUnits[attCount], fast);

            if (def > 0)
                temp.AddUnits(ListOfUnits[attCount + fastCount], def);

            ListOfUnits.RemoveRange(attCount + fastCount, def);
            ListOfUnits.RemoveRange(attCount, fast);
            ListOfUnits.RemoveRange(0, att);


            return temp;
        }

        public void SetBuff(float aBuff, float bBuff)
        {
            attDefBuff = aBuff;
            baseDefBuff = bBuff;
        }
            

        //Для ботов
        public Squad RaiseAttackingSquad()
        {
            Squad temp = new Squad(this.BasePos, this.TargetPos, this.CurrentPos, this.BaseNum);
            temp.SetBuff(this.attDefBuff, this.baseDefBuff);

            int attCount = ListOfUnits.Where(x => x.Att == 5).Count();
            int fastCount = ListOfUnits.Where(x => x.Att == 3).Count();
            int defCount = ListOfUnits.Where(x => x.Att == 2).Count();

            if (attCount > 0)
                temp.AddUnits(ListOfUnits[0], attCount);

            ListOfUnits.RemoveRange(0, attCount);

            return temp;
        }

        //Урон защитникам базы
        public void ReciveDamageDef(float count)
        {
            ListOfUnits = ListOfUnits.OrderBy(x => x.Def).ToList();

            while (count > ListOfUnits.Last().Def)
            {
                count -= ListOfUnits.Last().Def;
                ListOfUnits.RemoveAt(ListOfUnits.Count - 1);
            }
        }

        //Урон атакующему отряду
        public void ReciveDamageAtt(float count)
        {
            ListOfUnits = ListOfUnits.OrderBy(x => x.Att).ToList();

            while (count > ListOfUnits.Last().Att)
            {
                count -= ListOfUnits.Last().Att;
                ListOfUnits.RemoveAt(ListOfUnits.Count - 1);
            }
        }
    }
}
