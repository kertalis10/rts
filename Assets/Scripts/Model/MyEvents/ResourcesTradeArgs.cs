﻿using System;

namespace Assets.Scripts.Model.MyEvents
{
    public class ResourcesTradeArgs:EventArgs
    {
        public int SellCount { get; private set; }
        public int BuyCount { get; private set; }

        public ResourcesTradeArgs(int s, int b)
        {
            SellCount = s;
            BuyCount = b;
        }
    }
}
