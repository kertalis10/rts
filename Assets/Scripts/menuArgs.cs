﻿using System;

namespace Assets.Scripts
{
    public static class MenuArgs
    {
        public static int step;
        public static int size;
        public static int enemies;
        public static string reason;
        public static float time;
    }
}
