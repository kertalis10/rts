﻿using Start.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Start
{
    class Base
    {
        //определяет квадраты относящиеся к текущей базе
        public List<Point> Points { get; private set; }
        public Squad BaseSquad { get; set; }
        //идентификатор базы
        public int BaseNum { get; private set; }
        public List<int> BarracksLvl { get; private set; }
        public List<int> HouseLvl { get; private set; }
        public int WallLvl { get; private set; }
        public int FactoryLvl { get; private set; }
        public int PortalLvl { get; private set; }
        public int Peoples { get; private set; }
        public int Goods { get; private set; }
        public int Credits { get; private set; }
        public int PeoplesLimit { get; private set; }
        public int UnitsLimit { get; private set; }
        public float AttDefBuff { get; private set; }
        public float DefWallBuff { get; private set; }
        public bool IsSquadOnTheRaid { get; set; }


        int peoplesAddPerTurn;
        int goodsAddPerTurn;
        int creditsAddPerTurn;       




        public Base(int xStart, int yStart, int bNum)
        {
            Points = new List<Point>
            {
                new Point(xStart, yStart)
            };

            IsSquadOnTheRaid = false;

            BaseNum = bNum;

            BaseSquad = new Squad(Points[0], Points[0], Points[0], BaseNum);

            WallLvl = FactoryLvl = PortalLvl = 1;

            BarracksLvl = new List<int> { 1 };
            HouseLvl = new List<int> { 1 };

            Peoples = 200;
            Goods = 300;
            Credits = 500;
            peoplesAddPerTurn = goodsAddPerTurn = 100;
            creditsAddPerTurn = Peoples / 10;
            PeoplesLimit = 1000;
            UnitsLimit = 100;
            AttDefBuff = DefWallBuff = 0;           
        }

        public void AddUnits(Unit unit, int count)
        {
            if (UnitsLimit - BaseSquad.ListOfUnits.Count < count)
                count = UnitsLimit - BaseSquad.ListOfUnits.Count;

            if (Peoples - count < 0)
                count = Peoples;

            if (Goods - count * 10 < 0)
                count = Goods / 10;

            if (Credits - count * 10 < 0)
                count = Credits / 10;

            BaseSquad.AddUnits(unit, count);

            Peoples -= count;
            Credits -= count * 10;
            Goods -= count * 10;
        }        

        public bool IsCanAddBase()
        {
            if (Peoples >= 500 && Credits >= 1000 && Goods >= 1000)              
                return true;           
            return false;
        }

        public void AddBase(int x, int y)
        {
            Points.Add(new Point(x, y));
            WallLvl -= 5;
            if (WallLvl < 1) WallLvl = 1;
            Peoples -= 500;
            Credits -= 1000;
            Goods -= 1000;
            UnitsLimit += 200;
            PeoplesLimit += 1000;
            peoplesAddPerTurn += 100;
            goodsAddPerTurn += 100;
            HouseLvl.Add(1);
            BarracksLvl.Add(1);
        }

        public void Step()
        {
            Peoples += (int)(peoplesAddPerTurn * (1 + (HouseLvl.Sum(x => x) - HouseLvl.Count) * 0.05f));
            if (Peoples > PeoplesLimit)
                Peoples = PeoplesLimit;

            Goods += (int)(goodsAddPerTurn * (1 + (FactoryLvl - 1) * 0.0175f + (PortalLvl - 1) * 0.025f));

            creditsAddPerTurn = Peoples / 10;

            Credits += (int)(creditsAddPerTurn * (1 + (PortalLvl - 1) * 0.025f));
        }

        //Торговля
        public void Deal(int b, int s)
        {
            if (b > 0)
            {
                float byeRate = 1.5f - (PortalLvl - 1) * 0.005f;
                if (byeRate < 1) byeRate = 1;
                if (b > Credits * byeRate)
                    b = (int)(Credits / byeRate);

                Goods += b;
                Credits -= (int)(b * byeRate);
            }

            if (s > 0)
            {
                float sellRate = 0.5f + (PortalLvl - 1) * 0.005f;
                if (sellRate > 1) sellRate = 1;
                if (s > Goods)
                    s = Goods;

                Goods -= s;
                Credits += (int)(s * sellRate);
            }
        }

        public void WinSquadReturn()
        {
            Credits += 1000;
            Goods += 1000;    
        }

        public bool HouseUp()
        {
            int cost = BuildingUpCost(HouseLvl.Min());

            if (cost <= Goods && cost <= Credits)
            {
                HouseLvl = HouseLvl.OrderBy(x => x).ToList();
                HouseLvl[0]++;
                BuildingUp(cost);
                PeoplesLimit += 200;                
                return true;
            }
            return false;
        }

        public bool BarracksUp()
        {
            int cost = BuildingUpCost(BarracksLvl.Min());

            if (cost <= Goods && cost <= Credits)
            {
                BarracksLvl = BarracksLvl.OrderBy(x => x).ToList();
                BarracksLvl[0]++;
                BuildingUp(cost);
                UnitsLimit += 100;
                AttDefBuff += 0.1f;
                BaseSquad.SetBuff(AttDefBuff, DefWallBuff);
                return true;
            }
            return false;
        }

        public bool WallUp()
        {
            int cost = BuildingUpCost(WallLvl);

            if (cost <= Goods && cost <= Credits)
            {
                WallLvl++;
                BuildingUp(cost);
                DefWallBuff += 0.05f;
                BaseSquad.SetBuff(AttDefBuff, DefWallBuff);
                return true;
            }
            return false;
        }

        public bool FacktoryUp()
        {
            int cost = BuildingUpCost(FactoryLvl);

            if (cost <= Goods && cost <= Credits)
            {
                FactoryLvl++;
                BuildingUp(cost);               
                return true;
            }
            return false;
        }
                      
        public bool PortalUp()
        {
            int cost = BuildingUpCost(PortalLvl);

            if (cost <= Goods && cost <= Credits)
            {
                PortalLvl++;
                BuildingUp( cost);
                goodsAddPerTurn += (int)(goodsAddPerTurn * 0.0025f);
                creditsAddPerTurn = (int)((Peoples / 10) * 1 + ((PortalLvl - 1) * 0.05f));
                return true;
            }
            return false;
        }

        void BuildingUp(int cost)
        {          
            Goods -= cost;
            Credits -= cost;
        }

        public int BuildingUpCost(int buildingLvl)
        {
            int cost = (100 * buildingLvl) + (int)((buildingLvl + 1) / 0.985f);

            return cost;
        }

        public Squad RisePlayerAttack(int att, int fast, int def)
        {
            if (!IsSquadOnTheRaid)
            {
                IsSquadOnTheRaid = true;
                return BaseSquad.NewSquad(att, fast, def);
            }
            return null;
        }

        public override string ToString()
        {
            return  "P:" + Peoples + " C:" + Credits + " G:" + Goods + " B:" + BarracksLvl.Sum() + " H:" + HouseLvl.Sum() + " F:" + FactoryLvl + "CV:" + BaseSquad.ListOfUnits.Sum(x => x.Def) + "\n";
        }

        public string ResourcesInfo()
        {
            int ppt, gpt;

            ppt = (int)(peoplesAddPerTurn * (1 + (HouseLvl.Sum(x => x) - HouseLvl.Count) * 0.05f));
            gpt = (int)(goodsAddPerTurn * (1 + (FactoryLvl - 1) * 0.0175f + (PortalLvl - 1) * 0.025f));

            return "Peoples: " + Peoples + "(+" + ppt + ")/" + PeoplesLimit + " Credits: " + Credits + "(+" + Peoples/10 + ")" + " Goods: " + Goods + "(+" + gpt + ")" + " Units: " + BaseSquad.ListOfUnits.Count + "/" + UnitsLimit;           
        }
    }
}
