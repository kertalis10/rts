﻿using System;

namespace Assets.Scripts.Model.MyEvents
{
    public class PlayerAttackEventArgs:EventArgs
    {
        public int Att { get; private set; }
        public int Fast { get; private set; }
        public int Def { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }

        public PlayerAttackEventArgs(int att, int fast, int def, int x, int y)
        {
            Att = att;
            Fast = fast;
            Def = def;
            X = x;
            Y = y;
        }
    }
}
