﻿using Assets.Scripts.Model.MyEvents;
using Start.MyEvents;
using System;

namespace Start.View
{
    interface IView
    {
        event EventHandler<PlayerAttackEventArgs> PlayerAttack;
        event EventHandler<UnitsHireEventArgs> HireUnits;
        event EventHandler<StartEventArgs> StartGame;
        event EventHandler<BattleEventArgs> NewBattle;
        event EventHandler<ResourcesTradeArgs> NewDeal;
        event EventHandler<EventArgs> GameStep;
        event EventHandler<EventArgs> HouseUp;
        event EventHandler<EventArgs> BarracksUp;
        event EventHandler<EventArgs> WallUp;
        event EventHandler<EventArgs> FacktoryUp;
        event EventHandler<EventArgs> PortalUp;
        event EventHandler<EventArgs> ExpandBase;


        void Run();
        void DrawField(int[,] gameField, string[] s, int[] arr);

        void CheckTable(int[,] gameField, string[] s, int[] arr);

        void NewAttack(int xStart, int yStart, int xTarget, int yTarget, int baseNum, int speed);

        void DeleteSquad(int num);
        void Lose();
        void Win();
    }
}
