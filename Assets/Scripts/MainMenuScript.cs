﻿using Assets.Scripts;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using static Assets.Scripts.LoadSaveResult;

public class MainMenuScript : MonoBehaviour
{
    public int step;
    public int size;
    public int enemies;
    public Canvas main;
    public Canvas settings;
    public Canvas results;
    public Dropdown stepDD;
    public Dropdown sizeDD;
    public Dropdown enemiesDD;
    public Text res;


    // Start is called before the first frame update
    void Start()
    {
        ToMain();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && main.gameObject.activeInHierarchy == false)
            ToMain();
    }

    public void ToMain()
    {
        main.gameObject.SetActive(true);
        settings.gameObject.SetActive(false);
        results.gameObject.SetActive(false);
        step = Int32.Parse(stepDD.options[stepDD.value].text);
        size = Int32.Parse(sizeDD.options[sizeDD.value].text);
        enemies = Int32.Parse(enemiesDD.options[enemiesDD.value].text);
    }

    public void ToSettings()
    {
        main.gameObject.SetActive(false);
        settings.gameObject.SetActive(true);       
    }

    public void ToResults()
    {
        main.gameObject.SetActive(false);
        results.gameObject.SetActive(true);
        res.text = GetResult();
    }

    public void StartGame()
    {
        MenuArgs.enemies = enemies;
        MenuArgs.size = size;
        MenuArgs.step = step;
        SceneManager.LoadScene("1");
    }

    public void ToExit()
    {
        Application.Quit();
    }

    string GetResult()
    {
        List<PlayerInfo> results = LoadResults<PlayerInfo>("results.xml");

        string s = null;

        foreach (PlayerInfo pl in results)
            s += pl.ToString() + "\n";

        return s;
    }
}
