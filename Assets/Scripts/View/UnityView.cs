﻿using Assets.Scripts;
using Assets.Scripts.Model.MyEvents;
using Start;
using Start.MyEvents;
using Start.View;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnityView : MonoBehaviour, IView
{
    public Text goodsInPortal;
    public Text botInfoLbl;
    public Text statsLbl;
    public Text barracksLbl;
    public Text houseLbl;
    public Text wallLbl;
    public Text factoryLbl;
    public Text portalLbl;
    public Text attUnitLbl;
    public Text fastUnitLbl;
    public Text defUnitLbl;
    public Text attUnitSqLbl;
    public Text fastUnitSqLbl;
    public Text defUnitSqLbl;
    public GameObject panelRiseUnit;
    public GameObject panelEndGame;

    bool isFieldCreate, isUnitsHireThisTurn;
    float left, right, up, down, step;
    Pool[] bases;
    List<AttSquad> attSquads;
    float gameStepTime;
    float currentTime;

    //Для найма юнитов
    int credits;
    int goods;
    int peoples;
    int units;
    int attUnit;
    int fastUnit;
    int defUnit;
    int attUnitSq;
    int fastUnitSq;
    int defUnitSq;

    //для атаки базы игроком
    int attUnitBase;
    int fastUnitBase;
    int defUnitBase;
    Vector2 target;

    int goodsSelected;

    public event EventHandler<UnitsHireEventArgs> HireUnits;
    public event EventHandler<StartEventArgs> StartGame;
    public event EventHandler<PlayerAttackEventArgs> PlayerAttack;
    public event EventHandler<BattleEventArgs> NewBattle;
    public event EventHandler<ResourcesTradeArgs> NewDeal;
    public event EventHandler<EventArgs> GameStep;
    public event EventHandler<EventArgs> HouseUp;
    public event EventHandler<EventArgs> BarracksUp;
    public event EventHandler<EventArgs> WallUp;
    public event EventHandler<EventArgs> FacktoryUp;
    public event EventHandler<EventArgs> PortalUp;
    public event EventHandler<EventArgs> ExpandBase;

    void Start()
    {
        Time.timeScale = 1;
        MenuArgs.reason = "exit";
        MenuArgs.time = 0;
        gameStepTime = currentTime = MenuArgs.step;
        if(gameStepTime < 1)
            gameStepTime = currentTime = 1;

        goodsSelected = 0;
        isFieldCreate = isUnitsHireThisTurn = false;
        new GameModel(this.GetComponent<UnityView>());
    }

    private void GetCameraVisibility()
    {
        up = Camera.main.orthographicSize;
        down = -up;
        right = up / Camera.main.pixelHeight * Camera.main.pixelWidth;
        left = -right;
    }

    void FixedUpdate()
    {
        ShowUnitsHireInfo();       

        currentTime -= Time.deltaTime;
        if (currentTime < 0)
        {           
            currentTime = gameStepTime;
            isUnitsHireThisTurn = false;
            GameStep?.Invoke(this, new EventArgs());
            MenuArgs.time += gameStepTime;
        }


        //передвижение отрядов по карте
        foreach (AttSquad squad in attSquads)
        {
            if (squad.IsActive)
            {
                squad.Move(step);
            }

            if (squad.IsWaiting)
            {
                squad.IsWaiting = false;
                if (!squad.IsGoingHome)
                {
                    StartCoroutine(Battle(squad));
                    NewBattle?.Invoke(this, new BattleEventArgs(squad.BaseNum, squad.ColBaseNum + 1));
                }
                else
                {
                    NewBattle?.Invoke(this, new BattleEventArgs(squad.BaseNum, squad.ColBaseNum + 1));
                }
            }
        }
    }

    //Задержка после битвы
    IEnumerator Battle(AttSquad s)
    {       
        yield return new WaitForSeconds(0.5f);
        s.GoHome();
    }

    public void Run()
    {
        if(MenuArgs.size >= 10)
            StartGame?.Invoke(this, new StartEventArgs(MenuArgs.size, MenuArgs.size, MenuArgs.enemies, MenuArgs.step));
        else
            StartGame?.Invoke(this, new StartEventArgs(15, 15, 4, 4));

    }   

    public void DrawField(int[,] gameField, string[] s, int[] arr)
    {
        if (!isFieldCreate)
        {
            GetCameraVisibility();

            step = (up + (-down) - 2) / gameField.GetLength(0);

            for (int i = 0; i < gameField.GetLength(0); i++)
                for (int j = 0; j < gameField.GetLength(1); j++)
                {
                    GameObject newGameObject;
                    newGameObject = Instantiate(Resources.Load<GameObject>("Prefabs/Grass"));
                    newGameObject.transform.localScale = new Vector3(step, step, step);
                    newGameObject.transform.position = new Vector2(down + 1 + i * step, up - 1 - j * step);
                }

            isFieldCreate = true;

            bases = new Pool[6];
            bases[0] = new Pool("Prefabs/Base", 10, step);
            bases[1] = new Pool("Prefabs/Base1", 10, step);
            bases[2] = new Pool("Prefabs/Base2", 10, step);
            bases[3] = new Pool("Prefabs/Base3", 10, step);
            bases[4] = new Pool("Prefabs/Base4", 10, step);
            bases[5] = new Pool("Prefabs/Base5", 10, step);

            FillSquads();

            CheckTable(gameField, s, arr);

            ShowUnitsHireInfo();
        }
    }

    void FillSquads()
    {
        attSquads = new List<AttSquad>();
        string path = null;

        for (int i = 0; i < 6; i++)
        {
            switch (i)
            {
                case 0:
                    path = "Prefabs/Squad";
                    break;
                case 1:
                    path = "Prefabs/Squad1";
                    break;
                case 2:
                    path = "Prefabs/Squad2";
                    break;
                case 3:
                    path = "Prefabs/Squad3";
                    break;
                case 4:
                    path = "Prefabs/Squad4";
                    break;
                case 5:
                    path = "Prefabs/Squad5";
                    break;
                default:
                    break;
            }

            GameObject newGameObject;
            newGameObject = Instantiate(Resources.Load<GameObject>(path));
            newGameObject.transform.localScale = new Vector3(step / 3 * 2, step / 3 * 2, step / 3 * 2);
            newGameObject.SetActive(false);
            attSquads.Add(new AttSquad(newGameObject, i));
        }
    }  

    public void CheckTable(int[,] gameField, string[] s, int[] arr)
    {  
        foreach (Pool p in bases)
            p.Clear();

        for (int i = 0; i < gameField.GetLength(0); i++)
            for (int j = 0; j < gameField.GetLength(1); j++)
            {
                switch (gameField[i, j])
                {
                    case 1:
                        bases[0].GetObject(new Vector2(down + 1 + i * step, up - 1 - j * step));
                        break;
                    case 2:
                        bases[1].GetObject(new Vector2(down + 1 + i * step, up - 1 - j * step)).GetComponent<OnBaseCliskScript>().Click += BaseClick;                       
                        break;
                    case 3:
                        bases[2].GetObject(new Vector2(down + 1 + i * step, up - 1 - j * step)).GetComponent<OnBaseCliskScript>().Click += BaseClick;
                        break;
                    case 4:
                        bases[3].GetObject(new Vector2(down + 1 + i * step, up - 1 - j * step)).GetComponent<OnBaseCliskScript>().Click += BaseClick;
                        break;
                    case 5:
                        bases[4].GetObject(new Vector2(down + 1 + i * step, up - 1 - j * step)).GetComponent<OnBaseCliskScript>().Click += BaseClick;
                        break;
                    case 6:
                        bases[5].GetObject(new Vector2(down + 1 + i * step, up - 1 - j * step)).GetComponent<OnBaseCliskScript>().Click += BaseClick;
                        break;
                    default:

                        break;
                }
            }

        botInfoLbl.text = s[0];
        statsLbl.text = s[1];
        houseLbl.text = s[2];
        barracksLbl.text = s[3];
        wallLbl.text = s[4];
        factoryLbl.text = s[5];
        portalLbl.text = s[6];

        credits = arr[0];
        goods = arr[1];
        peoples = arr[2];
        units = arr[3];
        attUnitBase = arr[4];
        fastUnitBase = arr[5];
        defUnitBase = arr[6];        
    }

    public void DeleteSquad(int num)
    {
        attSquads[num].SetOff();      
    }

    public void Win()
    {
        MenuArgs.reason = "Win";
        Time.timeScale = 0;
        panelEndGame.SetActive(true); 
        
    }

    public void Lose()
    {
        MenuArgs.reason = "Lose";
        Time.timeScale = 0;
        panelEndGame.SetActive(true);
    }

    /**********************************************************************************/
    //Улучшение стройний, расширения базы
    public void HouseUpDo()
    {
        HouseUp?.Invoke(this, new EventArgs());
    }

    public void BarracksDo()
    {
        BarracksUp?.Invoke(this, new EventArgs());
    }

    public void WallUpDo()
    {
        WallUp?.Invoke(this, new EventArgs());
    }

    public void FactoryUpDo()
    {
        FacktoryUp?.Invoke(this, new EventArgs());
    }

    public void PortalUpDo()
    {
        PortalUp?.Invoke(this, new EventArgs());
    }

    public void ExpandBaseDo()
    {
        ExpandBase?.Invoke(this, new EventArgs());
    }

    /***************************************************************************/
    //Найм юнитов на базу
    public void AttUnitPlus()
    {
        if (!isUnitsHireThisTurn && defUnit == 0 && fastUnit == 0)
        {
            if (peoples - attUnit > 0 && units - attUnit > 0 && credits - attUnit * 10 > 0 && goods - attUnit * 10 > 0)
            {
                attUnit++;
                ShowUnitsHireInfo();
            }
        }
    }

    public void DefUnitPlus()
    {
        if (!isUnitsHireThisTurn && attUnit == 0 && fastUnit == 0)
        {
            if (peoples - defUnit > 0 && units - defUnit > 0 && credits - defUnit * 10 > 0 && goods - defUnit * 10 > 0)
            {
                defUnit++;
                ShowUnitsHireInfo();
            }
        }
    }

    public void FastUnitPlus()
    {
        if (!isUnitsHireThisTurn && defUnit == 0 && attUnit == 0)
        {
            if (peoples - fastUnit > 0 && units - fastUnit > 0 && credits - fastUnit * 10 > 0 && goods - fastUnit * 10 > 0)
            {
                fastUnit++;
                ShowUnitsHireInfo();
            }
        }
    }
    public void CancelUnits()
    {
        fastUnit = attUnit = defUnit = 0;
        ShowUnitsHireInfo();
    }

    public void HireUnit()
    {
        HireUnits?.Invoke(this, new UnitsHireEventArgs(attUnit, fastUnit, defUnit));

        isUnitsHireThisTurn = true;

        CancelUnits();       
    }

    private void ShowUnitsHireInfo()
    {
        attUnitLbl.text = "Attack\n Units \n(5, 1, 2):\n" + attUnit + "(" + attUnitBase + ")";
        fastUnitLbl.text = "Fast\n Units \n(3, 1, 4):\n" + fastUnit + "(" + fastUnitBase + ")";
        defUnitLbl.text = "Defence\n Units \n(4, 2, 2):\n" + defUnit + "(" + defUnitBase + ")";
        attUnitSqLbl.text = "Attack\n Units \n(5, 1, 2):\n" + attUnitSq + "(" + attUnitBase + ")";
        fastUnitSqLbl.text = "Fast\n Units \n(3, 1, 4):\n" + fastUnitSq + "(" + fastUnitBase + ")";
        defUnitSqLbl.text = "Defence\n Units \n(4, 2, 2):\n" + defUnitSq + "(" + defUnitBase + ")";
    }

    /********************************************************************************************/
    //Формирование атакующего отряда

    private void BaseClick(object sender, BaseClickArgs e)
    {
        fastUnitSq = attUnitSq = defUnitSq = 0;
        ShowUnitsHireInfo();
        panelRiseUnit.SetActive(true);
        target = new Vector2(e.X, e.Y);
    }

    public void CancelRise()
    {
        fastUnitSq = attUnitSq = defUnitSq = 0;
        ShowUnitsHireInfo();
        panelRiseUnit.SetActive(false);
    }

    public void Rise()
    {
        if(attUnitSq + fastUnitSq + defUnitSq > 0)
        {
            int x = (int)Math.Round((target.x - down - 1) / step);
            int y = (int)Math.Round((up - 1 - target.y) / step);
            panelRiseUnit.SetActive(false);
            PlayerAttack?.Invoke(this, new PlayerAttackEventArgs(attUnitSq, fastUnitSq, defUnitSq, x, y));
            ShowUnitsHireInfo();
        }
    }

    public void RiseOneAtt()
    {
        if (attUnitSq < attUnitBase)
            attUnitSq++;
        ShowUnitsHireInfo();
    }

    public void RiseAllAtt()
    {
        attUnitSq = attUnitBase;
        ShowUnitsHireInfo();
    }

    public void RiseOneFast()
    {
        if (fastUnitSq < fastUnitBase)
            fastUnitSq++;
        ShowUnitsHireInfo();
    }

    public void RiseAllFast()
    {
        fastUnitSq = fastUnitBase;
        ShowUnitsHireInfo();
    }

    public void RiseOneDef()
    {
        if (defUnitSq < defUnitBase)
           defUnitSq++;
        ShowUnitsHireInfo();
    }

    public void RiseAllDef()
    {
        defUnitSq = defUnitBase;
        ShowUnitsHireInfo();
    }

    public void NewAttack(int xStart, int yStart, int xTarget, int yTarget, int baseNum, int speed)
    {
        attSquads[baseNum].ChangeTargetPos(false, new Vector2(down + 1 + xStart * step, up - 1 - yStart * step), new Vector2(down + 1 + xTarget * step, up - 1 - yTarget * step));
        attSquads[baseNum].Speed = speed;
    }


    /***************************************************************************************************/
    //Торговля на портале
    public void PlusGoods()
    {
        goodsSelected += 10;
        goodsInPortal.text = goodsSelected.ToString();
    }

    public void MinusGoods()
    {
        if (goodsSelected > 10)
        {
            goodsSelected -= 10;
            goodsInPortal.text = goodsSelected.ToString();
        }
    }

    public void BuyGoods()
    {
        NewDeal?.Invoke(this, new ResourcesTradeArgs(0, goodsSelected));
        goodsSelected = 0;
        goodsInPortal.text = goodsSelected.ToString();
    }

    public void SellGoods()
    {
        NewDeal?.Invoke(this, new ResourcesTradeArgs(goodsSelected, 0));
        goodsSelected = 0;
        goodsInPortal.text = goodsSelected.ToString();
    }

    //Пул объектов
    class Pool
    {
        float size;
        int num;
        string path;
        List<GameObject> list;

        public Pool(string _path, int count, float _size)
        {
            num = 0;
            list = new List<GameObject>(count);
            path = _path;
            size = _size;

            for (int i = 0; i < count; i++)
            {
                GameObject newGameObject;
                newGameObject = Instantiate(Resources.Load<GameObject>(path));
                newGameObject.transform.localScale = new Vector3(size, size, size);
                newGameObject.SetActive(false);
                list.Add(newGameObject);
            }
        }

        public GameObject GetObject(Vector3 v3)
        {
            if (num < list.Count)
            {
                list[num].SetActive(true);
                list[num].transform.position = v3;
                num++;
            }
            else
            {
                int temp = list.Count;
                for (int i = 0; i < temp; i++)
                {
                    GameObject newGameObject;
                    newGameObject = Instantiate(Resources.Load<GameObject>(path));
                    newGameObject.transform.localScale = new Vector3(size, size, size);
                    newGameObject.SetActive(false);
                    list.Add(newGameObject);
                }
                list[num].SetActive(true);
                list[num].transform.position = v3;
                num++;
            }
            return list[num - 1];
        }
        public void Clear()
        {
            foreach (GameObject go in list)
                go.SetActive(false);

            num = 0;
        }
    }

    //Отряды на карте
    class AttSquad
    {
        public int ColBaseNum;
        public int BaseNum { get; private set; }
        public bool IsWin { get; private set; }
        public bool IsActive { get; private set; }
        public bool IsGoingHome { get; private set; }
        public bool IsWaiting { get; set; }
        public GameObject Squad { get; private set; }
        public Vector2 Target { get; private set;}
        public Vector2 Home { get; private set; }
        public int Speed { get; set; }


        public AttSquad(GameObject go, int bs)
        {
            BaseNum = bs;
            Squad = go;
            ColBaseNum = 100;
            go.GetComponent<SquadScript>().NewCollision += NewCollision; 
            IsWin = IsActive = IsGoingHome = IsWaiting = false;
        }

        public void ChangeTargetPos(bool isGH, Vector2 basicPos, Vector2 targetPos)
        {
            Squad.transform.position = basicPos;
            Home = basicPos;
            Target = targetPos;            
            IsGoingHome = isGH;
            Squad.SetActive(true);
            IsActive = true;
        }

        public void SetOff()
        {
            Squad.SetActive(false);
            IsWin = IsActive = IsGoingHome = IsWaiting = false;
        }

        public void GoHome()
        {
            Target = Home;
            IsGoingHome = true;
            IsWaiting = false;
        }

        public void Move(float step)
        {
            Squad.transform.position = Vector2.MoveTowards(Squad.transform.position, Target, step * Time.deltaTime * Speed) ;
            if (Squad.transform.position == new Vector3(Target.x, Target.y, 0))
                IsWaiting = true;                
        }

        void NewCollision(string s)
        {
            if (int.Parse(s) != BaseNum)
            {
                ColBaseNum = int.Parse(s);
                IsWaiting = true;                
            }

            if (int.Parse(s) == BaseNum && IsGoingHome)
            {
                ColBaseNum = int.Parse(s);
                IsWaiting = true;
            }
        }
    }
    
}


