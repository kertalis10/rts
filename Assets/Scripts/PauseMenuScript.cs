﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;
using static Assets.Scripts.LoadSaveResult;
using UnityEngine.UI;
using Assets.Scripts;

public class PauseMenuScript : MonoBehaviour
{
    public Canvas mainCanvas;
    public Canvas pauseMenu;
    public Canvas saveMenu;
    public InputField name;
    bool toMainMenu;
    bool toExit;

    void Start()
    {
        toMainMenu = true;
        toExit = false;
        ToMain();
    }

    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
    }

    public void Pause()
    {
        if (mainCanvas.gameObject.activeInHierarchy == true)
        {
            ToPauseMenu();
            Time.timeScale = 0;
        }
        else if(saveMenu.gameObject.activeInHierarchy == false)
        {
            ToMain();
            Time.timeScale = 1;
        }
    }

    public void ToMain()
    {
        mainCanvas.gameObject.SetActive(true);
        pauseMenu.gameObject.SetActive(false);
        saveMenu.gameObject.SetActive(false);        
    }

    public void ToPauseMenu()
    {
        mainCanvas.gameObject.SetActive(false);
        pauseMenu.gameObject.SetActive(true);
        saveMenu.gameObject.SetActive(false);
    }

    public void ToResultMenu()
    {
        mainCanvas.gameObject.SetActive(false);
        pauseMenu.gameObject.SetActive(false);
        saveMenu.gameObject.SetActive(true);
    }

    public void ToMainMenu()
    {
        toMainMenu = true;
        ToResultMenu();
    }

    public void ToExit()
    {
        toExit = true;
        ToResultMenu();
    }

    public void Yes()
    {
        Save();
        if(toMainMenu)
            SceneManager.LoadScene("0");
        if(toExit)
            Application.Quit();
    }

    public void No()
    {
        if (toMainMenu)
            SceneManager.LoadScene("0");
        if (toExit)
            Application.Quit();
    }

    private void Save()
    {
        List<PlayerInfo> results = LoadResults<PlayerInfo>("results.xml");

        PlayerInfo pI = new PlayerInfo(name.text, MenuArgs.reason, MenuArgs.enemies, MenuArgs.time);

        results.Add(pI);

        results = results.OrderByDescending(x => x.time).ToList();

        SaveResults<PlayerInfo>(results, "results.xml");
    }

}
